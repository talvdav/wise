$computer = gwmi win32_computersystem
$bios = gwmi win32_bios
$os = (gwmi win32_operatingsystem).caption
$serial = $bios.serialnumber
$colCPUs = gwmi win32_processor

$n = 0
foreach ($cpu in $colCPUs)
{
    $n = $n+1
    $processor = ((($cpu.name -replace ' {2,}', ' ') -replace "\(TM\)") -replace '\(R\)')
}
$cpuspeed = "{0:0.00 GHz}" -f($cpu.maxclockspeed/1000)

$physmem = ([int]($computer.TotalPhysicalMemory /1MB))
$disk = gwmi win32_diskdrive -filter "InterfaceType <> 'USB'"

$disksize=""
foreach ($objitem in $disk)
{
    $disksize=$disksize+";"+[Convert]::ToString(([int]($objitem.size/1000000000)))
}
$disksize = $disksize.substring(1)

$lancount = gwmi win32_networkadapterconfiguration -filter "ipenabled=true"
$ip=""
foreach($lan in $lancount)
{
    $ip = $ip + ";" + [string]($lan.ipaddress[0])
}
$ip = $ip.substring(1)

$disk = Get-PhysicalDisk

write-host "Computer Name:   " $computer.name
write-host "Computer Model:  " $computer.model
write-host "Serial:          " $serial
write-host "Processor:       " $processor
write-host "Clock speed:     " $cpuspeed
write-host "Physical Memory: " $physmem
write-Host "Disk Name:       " $disk.FriendlyName
write-host "Disk Type:       " $disk.MediaType
write-host "Disk Size:       " $disksize
write-host "Disk Serial:     " $disk.SerialNumber
write-host "OS:              " $os
write-host "Ip:              " $ip
