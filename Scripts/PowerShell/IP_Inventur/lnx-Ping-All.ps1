
Function Ping-All {
    $ips = @();
    0..255 | foreach { $ips += "192.168.178." + $_ }

    $LogFileName = (Get-Date -Format "yyyyMMdd_HHmmss").ToString()
    $LogFileName += ".log"
    foreach ($ip in $ips) {
        $datetime = (Get-Date -Format "yyyyMMdd HHmmss").ToString()
        $NetInfo = GetNetInfo $ip

        write-host $NetInfo.IP $NetInfo.Hostname $NetInfo.Domain $NetInfo.Ping
    }
}
