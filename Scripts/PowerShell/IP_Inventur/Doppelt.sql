WITH doubled AS 
(SELECT hostname FROM netinfo 
GROUP BY hostname HAVING COUNT(hostname) > 1 AND hostname!="none")
SELECT datetime,hostname,ip,ping FROM netinfo WHERE hostname IN doubled 
ORDER BY hostname 
