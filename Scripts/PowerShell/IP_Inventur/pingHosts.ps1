Import-Module PSSQLite

Function GetHostByAddress {
    Param ([string]$IP)

    try {
        $myHost = [System.Net.DNS]::GetHostEntry($IP).Hostname
    } catch {
        $myHost = "none"
    }
    $myHost
}

Function TestConnection {
    Param ([string]$IP)
    $Res = Test-Connection $IP -quiet -count 1
}

Function GetNetInfo {
    Param ([string]$IP)

    $Res = @{Hostname = " "
             Domain   = " "
             Ping     = " "
             Ip       = " "}

    $FQDN = GetHostByAddress ($IP)
    $Res.Hostname = $FQDN.Split(".")[0]
    $Res.Domain = $FQDN.Split(".")[1] + "." + $FQDN.Split(".")[2]
    $Res.Ping = Test-Connection $IP -quiet -count 1
    if ($Res.Ping -eq "True"){
        $Res.Ping = "Ok"
    } else {
        $Res.Ping = "Failed"
    }
    $Res.IP = $IP
    $Res
}

Function CreateSqliteDatabase {
    Param([string]$DB)
    $Query = "CREATE TABLE netinfo ( datetime TEXT 
                                     , ip TEXT
                                     , hostname TEXT
                                     , domain TEXT
                                     , ping TEXT)"

    Invoke-SqliteQuery -DataSource $DB -Query $Query
}

Function Ping-All {
    Param([string]$IP)
    if (!$IP) {
	$IP = "192.168.0."
    } else {
	$pos = $IP.LastIndexOf(".")
	$IP = $IP.Substring(0,$pos + 1)
    }
    
    $ips = @();
    0..255 | foreach { $ips += $IP + $_ }
    # 0..255 | foreach { $ips += "192.168.2." + $_ }

    $LogFileName = (Get-Date -Format "yyyyMMdd_HHmmss").ToString()
    $LogFileName += ".log"
    $DB = ".\" + $LogFileName + ".sqlite"

    if (!(Test-Path "$DB")) {
         CreateSqliteDatabase($DB)
    }

    foreach ($ip in $ips) {
    
        $datetime = (Get-Date -Format "yyyyMMdd HHmmss").ToString()
        $NetInfo  = GetNetInfo $ip
        $Query    = "INSERT INTO netinfo (datetime, ip, hostname, domain, ping) VALUES (@datetime, @ip, @hostname, @domain, @ping)"

        Invoke-SqliteQuery -DataSource $DB -Query $Query -SqlParameters @{
            datetime = $datetime
            ip       = $NetInfo.IP
            hostname = $NetInfo.Hostname
            domain   = $NetInfo.Domain
            ping     = $NetInfo.ping
        }
        write-host $NetInfo.IP $NetInfo.Hostname $NetInfo.Domain $NetInfo.Ping
    }
}

Function Get-Duplicates {
    Param([string]$File)
    if(Test-Path "$File") {
        $Query ="WITH doubled AS (SELECT hostname FROM netinfo GROUP BY hostname HAVING COUNT(hostname) > 1 AND hostname !='none') SELECT ip,hostname,ping FROM netinfo WHERE hostname IN doubled;"
        Invoke-SqliteQuery -DataSource $File -Query $Query
    } else {
        write-host $File
        write-host "File not found"
    }
}

